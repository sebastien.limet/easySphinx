# Exemple pour des énoncés utilisant le module de test de code pour sphinx. 


## Installation : 

Il faut avoir sphinx_bootstrap_theme, par exemple en faisant :

    $ pip3 install sphinx_bootstrap_theme

Puis cloner le dépot : 

    $ git clone --recurse https://gitlab.com/sebastien.limet/easySphinx.git

puis dans le dossier easySphinx: 

    $ make html

