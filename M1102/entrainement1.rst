Entrainement Série 1
********************

les basiques
============

.. include::  exercices/td2/variantes/appartient_nb.rst
.. include::  exercices/td2/variantes/maximum.rst


avec des conditionnelles
========================

.. include::  exercices/entrainement/conditionnelles/bissextile.rst
.. include::  exercices/entrainement/conditionnelles/chauffage.rst


D'autres
========

.. include::  exercices/td2/variantes/appartient_str.rst
