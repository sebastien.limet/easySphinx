TD4 Les boucles while
*********************


.. include::  exercices/td4/niemeOccurrence.rst
.. include::  exercices/td4/estSymetrique.rst
.. include::  exercices/td4/nombreChiffres.rst
.. include::  exercices/td4/nombreToListe.rst
.. include::  exercices/td4/deuxChiffresConsecutifsEgaux.rst
.. include::  exercices/td4/nombreEstSymetrique.rst

	      
