TP Les Chaînes de caractères: Cryptage
**************************************

On voudrait écrire un programme de cryptage de texte. La clé sera
représentée par une chaîne de caractères contenant les 26 lettres de
l'alphabet ainsi que le caractère espace (pas obligatoirement dans
l'ordre). Le texte crypté sera construit à partir du texte original
grâce à cette clé. Voici un exemple de code: *'nbvcxwmlkjhgfdsqpoiuy~treza'*.
Le texte crypté sera une chaîne de
caractères ne contenant que des chiffres. Nous allons décrire la
méthode de cryptage et de décryptage ci-dessous. A chaque caractère de
l'alphabet sera attribué un code numérique (c-à-d un nombre) qui ne
sera ni 0 ni un multiple de 10. Ce code est calculé en fonction de la
clé de cryptage de la manière suivante:

- On récupère l'indice *i* du caractère dans la clé

- on ajoute à *i* le nombre de fois plus 1 que *i* est divisible
  par 9

Ainsi le code de la lettre *'n'* dans la clé précédente est 1 car
l'indice de *'n'* est 0 dans la clé et que l'on a 0 fois 9
dans 0 donc 0+0+1=1. Le code de la lettre *'a'* est 29 car
l'indice de *'a'* est 26 et que on a 2 fois 9 dans 26 donc
26+2+1=29.

Pour encrypter un texte nous allons construire une chaîne de
caractères qui va être constituée de la transformation des codes de
chacun des caractères du texte en chaîne de caractères, chaque code étant
séparé par le caractère *'0'*. Par exemple le texte *'bonjour'* sera crypté en
*'0201901011019022026'* si on utilise
la clé définie plus haut. En effet si on découpe texte crypté suivant
les *'0'* on obtient la correspondance suivante:

   +----+-----+----+-----+-----+-----+-----+
   |'b' | 'o' | 'n'| 'j' | 'o' | 'u' | 'r' |
   +----+-----+----+-----+-----+-----+-----+
   |'02'|'019'|'01'|'011'|'019'|'022'|'026'|
   +----+-----+----+-----+-----+-----+-----+

avec 2 qui est le code de *'b'*, 19 celui de *'o'*
etc. Pour l'encryptage on utilisera la fonction *str* qui permet
de transformer un nombre en une chaîne de caractères. Par exemple *str(12)* retourne *'12'*. Pour la fonction de décryptage, on
utilisera la fonction *int*  qui permet de transformer une chaîne
de caractères contenant un nombre en un *int*. Par exemple *int('23')* retourne *23*. Pour le décryptage, il faut remarquer
que pour retrouver l'indice du caractère dans la clé à partir du code,
il suffit de retirer à ce code le nombre de fois plus 1 qu'il contient 10. Par exemple la lettre correspondant au code 26 se trouve à
l'indice 26-(2+1)=23 car 26 contient 2 fois 10.

Nous voulons donc écrire une fonction d'encryptage et une fonction de
décryptage de texte. Pour y arriver, on va avoir besoin d'un certain
nombre de fonctions qui vont résoudre une partie du problème.

.. include::  exercices/tpchaines/codeLettre.rst
.. include::  exercices/tpchaines/decodeLettre.rst
.. include::  exercices/tpchaines/crypter.rst
.. include::  exercices/tpchaines/decrypter.rst
