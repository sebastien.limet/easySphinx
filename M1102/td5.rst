TD5 Les differents types de boucles
***********************************

.. include::  exercices/td5/cumul.rst
.. include::  exercices/td5/investissement.rst
.. include::  exercices/td5/correction.rst
.. include::  exercices/td5/majoritePaire.rst
.. include::  exercices/td5/rupture.rst
