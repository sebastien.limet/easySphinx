Indice de la nième occurrence
=============================

On voudrait récupérer l'indice de la nième occurrence d'un élément d'une liste.
Par exemple, si je veux la troisième occurrence du nombre 12 dans la liste [12,3,4,12,8,12,5,12]
on souhaite obtenir 5 car c'est l'indice de la 3ème occurrence de 12 dans la liste.
Si l'élément apparait moins de fois que ce qui est recherché, on retourne la valeur -1.


.. easypython:: exercices/td4/niemeOccurrence.py
   :language: python
   :uuid: 1231313
