Nombre de chiffres d'un nombre
==============================

Ecrire une fonction qui compte le nombre de chiffres d'un nombre entier (positif).

On rappelle que le chiffre des unités d'un nombre est obtenu en lui appliquant ``%10``
et que le nombre sans ses unités est obtenu en lui appliquant ``//10``.



.. easypython:: exercices/td4/nombreChiffres.py
   :language: python
   :uuid: 1231313
