entrees_visibles = [(1234,),
                    (0,),
]
entrees_invisibles = [
        (1,),
        (1235,),
]

@solution
def nombreChiffres(nombre):
  """
  """ 
  return len(str(nombre))
