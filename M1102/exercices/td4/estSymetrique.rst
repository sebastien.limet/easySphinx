Liste symétrique
================

\'Ecrire une fonction qui vérifie si une liste est symétrique. 


.. easypython:: exercices/td4/estSymetrique.py
   :language: python
   :uuid: 1231313
