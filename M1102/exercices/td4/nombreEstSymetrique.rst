Nombre symétrique
=================

On voudrait savoir si un nombre (positif) est symétrique ou non.

\'Ecrire une fonction qui résoud ce problème.

.. easypython:: exercices/td4/nombreEstSymetrique.py
   :language: python
   :uuid: 1231313
