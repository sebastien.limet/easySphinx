Deux chiffres consecutifs égaux
===============================

On voudrait savoir si un nombre (positif) contient deux chiffres consécutifs égaux.

\'Ecrire une fonction qui résoud ce problème.

.. easypython:: exercices/td4/deuxChiffresConsecutifsEgaux.py
   :language: python
   :uuid: 1231313
