Moyenne
=======

Ecrire une fonction renvoie la moyenne des nombres dans une liste de nombres. 


.. easypython:: exercices/td2/moyenne.py
   :language: python
   :uuid: 1231313
