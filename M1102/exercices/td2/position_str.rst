Position
========

Ecrire une fonction renvoie la  position  de  la  première  apparition  de  la  lettre  (la  première  lettre  du  mot  ayant  la
position 0). Si la lettre n’apparait pas dans le mot on souhaite obtenir le résultat -1

.. easypython:: exercices/td2/position_str.py
   :language: python
   :uuid: 1231313
