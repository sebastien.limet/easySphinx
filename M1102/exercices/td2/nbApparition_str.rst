Nombre occurrences lettre
=========================

Ecrire une fonction renvoie Le nombre de fois où une lettre apparait dans un mot


.. easypython:: exercices/td2/nbApparition_str.py
   :language: python
   :uuid: 1231313
