Une lettre appartient à un mot
------------------------------

Ecrire une fonction permettant de savoir si une lettre appartient à un mot. 


.. easypython:: exercices/td2/variantes/appartient_str.py
   :language: python
   :uuid: 1231313
