Maximum
-------

Ecrire une fonction renvoie le plus grand nombre d'une liste de nombres. 


.. easypython:: exercices/td2/variantes/maximum.py
   :language: python
   :uuid: 1231313
