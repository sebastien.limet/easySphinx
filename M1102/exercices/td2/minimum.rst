Minimum
=======

Ecrire une fonction renvoie le plus petit nombre d'une liste de nombres. 


.. easypython:: exercices/td2/minimum.py
   :language: python
   :uuid: 1231313
