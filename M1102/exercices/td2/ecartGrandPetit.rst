Ecart entre le plus grand et le plus petit
==========================================

Ecrire une fonction renvoie l'écart entre le plus grand nombre et le plus petit nombre  dans une liste. 


.. easypython:: exercices/td2/ecartGrandPetit.py
   :language: python
   :uuid: 1231313
