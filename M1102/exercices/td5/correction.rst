Notation automatique
--------------------

Dans le cadre d'un logiciel de correction automatique de
questionnaire, on vous demande d'écrire une fonction qui indique le
nombre de bonnes réponses fournies par un étudiant. Pour faire cette
correction, on dispose de la liste des réponses données par l'étudiant
et la liste des bonnes réponses. Ces deux listes ont obligatoirement la
même longueur.

.. easypython:: exercices/td5/correction.py
   :language: python
   :uuid: 1231313
