entrees_visibles = [
        ([4,5,-3,1,6,2,-3],10),
        ([4,5,-3,1,6,2,-3],40)
]
entrees_invisibles = [
        ([4,5,-3,1,6,2,-3],1),
        ([1,2,1,3],40)
]

@solution
def cumul(liste,valeur):
    """
    recherche l'indice tel que la somme des éléments de la liste du début
    jusqu'à cet indice dépasse la valeur.
    paramètres: liste une liste de nombres
                valeur le cumul à dépasser
    retour l'indice rechercher ou -1 si il n'existe pas
    """
    # je choisis une boucle while pour m'arrêter dès que possible
    somme=0
    i=0
    while i<len(liste) and somme<valeur:
        #somme est la somme des i premiers éléments de la liste
        somme=somme+liste[i]
        i=i+1
    if somme>=valeur:
        res=i-1
    else:
        res=-1
    return res
