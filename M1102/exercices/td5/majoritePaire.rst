Majorité de nombres paires
--------------------------
On voudrait une fonction qui nous indique si une liste de nombres
contient plus de nombre paires que de nombre impaires. La fonction
doit retourne 1 si c'est le cas, -1 si ce sont les nombres impaires
qui sont majoritaires et 0 sinon. Par exemple pour la liste
[-6,4,3,1,0] on doit obtenir 1.


.. easypython:: exercices/td5/majoritePaire.py
   :language: python
   :uuid: 1231313
