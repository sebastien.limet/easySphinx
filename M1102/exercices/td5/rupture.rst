Rupture dans une liste de nombres
---------------------------------

Dans une liste de nombres on va définir une rupture comme
étant une valeur dont la différence avec son précédent dans la
liste dépasse un certain seuil. Par exemple dans la liste
[1,5,-3,40,32] avec le seuil de 10 la valeur 40 est une rupture sa
différence avec -3 dépasse le seuil.
On voudrait une fonction qui retourne l'indice de la première rupture
de la liste (-1 s'il n'y a pas de rupture)


.. easypython:: exercices/td5/rupture.py
   :language: python
   :uuid: 1231313
