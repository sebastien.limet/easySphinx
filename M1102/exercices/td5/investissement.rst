Placement financier
-------------------

Dans cadre d'un logiciel bancaire, on vous demande d'écrire une
fonction qui permet de calculer le nombre d'années
nécessaires pour un client qui place une certaine somme d'argent sur
un produit qui a un taux d'intérêt annuel $t$ pour acquérir un certain
capital. Par exemple le client place 1000€ sur un placement à 5%
annuel et il veut obtenir un capital de 1100€ il lui faudra 2 ans. En
effet la première année il aura un capital de **1000+1000*5/100**
soit 1050€ et la deuxième année il aura un capital de **1050+1050*5/100**
soit 1102.5€. Attention si la somme initiale ou le taux sont inférieurs ou
égale à 0 on veut obtenir **None**.

.. easypython:: exercices/td5/investissement.py
   :language: python
   :uuid: 1231313
