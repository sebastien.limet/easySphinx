Exercice 5.2
------------

On veut comparer deux chaines de caractères en utilisant **uniquement** la comparaison entre deux caractères.
La fonction devrat retourner -1 si la première chaine est avant la seconde dans l'ordre alphabétique,
0 si elles sont égales et 1 sinon.

.. easypython:: exercices/tp5/compareChaines.py
   :language: python
   :uuid: 1231313
