Exercice 3.2
------------
Écrire une fonction qui fait la somme des n premiers entiers. Par
exemple si n vaut 4 le résultat doit être 10 qui est le résultat de 1+2+3+4.

.. easypython:: exercices/tp3/exo32.py
   :language: python
   :uuid: 1231313
