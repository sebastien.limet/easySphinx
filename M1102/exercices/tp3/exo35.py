entrees_visibles = [([6,6,3,4,2,2,2,4],),
                    ([1,2,3,4,5],),
                    ([],)
]
entrees_invisibles = [
        ([1,1,2,3,4,5,5,5,5],),
        ([1,2,2,4,3,5,4],),
        ([1,1,2,2,3,3,4,4],)
]

@solution
def plateau(liste):
    """
    indique si longueur de la plus grande suite de nombres égaux dans la liste
    paramètre: liste: une liste de nombres
    résultat: le nombre d'éléments de la plus grande suite de nombres égaux dans la liste
    """ 

    maxi=0
    lg=0
    prec=None
    for n in liste:
        if n==prec:
            lg+=1
        else:
            if lg > maxi:
                maxi=lg
            lg=1
        prec=n
    if lg>maxi:
        maxi=lg
    return maxi
