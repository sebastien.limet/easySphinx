Exercice 3.1
------------

Écrire une fonction qui fait la somme des nombres paires d’une liste
de nombres. Par exemple si la liste est [12,13,6,5,7] le résultat doit
être 18 car seuls 12 et 6 sont paires

.. easypython:: exercices/tp3/exo31.py
   :language: python
   :uuid: 1231313
