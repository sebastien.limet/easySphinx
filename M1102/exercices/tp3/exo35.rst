Exercice 3.5
------------

Écrire une fonction qui permet de connaı̂tre dans une liste de nombres,
la lon- gueur de la plus grande suite de nombres consécutifs
égaux. Par exemple sur la liste [6,6,3,4,2,2,2,4] on doit trouver 3
car la plus grande suite contient trois 2 consécutifs.

.. easypython:: exercices/tp3/exo35.py
   :language: python
   :uuid: 1231313
