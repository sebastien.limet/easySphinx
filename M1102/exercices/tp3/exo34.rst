Exercice 3.4
------------

Écrire une fonction qui permet de savoir si
un mot contient au moins deux lettres consécutives identiques. Par
exemple *coucou* ne répond pas au critère alors que *creee* si.

.. easypython:: exercices/tp3/exo34.py
   :language: python
   :uuid: 1231313
