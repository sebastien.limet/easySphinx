Exercice 3.3
------------
Écrire une fonction qui fait la somme des
entiers paires parmi les n premiers entiers. Par exemple si n vaut 4
le résultat doit être 6 qui est le résultat de 2+4.

.. easypython:: exercices/tp3/exo33.py
   :language: python
   :uuid: 1231313
