Exercice 1
----------

Écrire une fonction qui détermine si un parallépipède est un carré un
losange, un rectangle ou quelconque.
On vous rappelle que :

- les cotés d'un rectangle se coupent avec un angle droit.
- les cotés d'un losange ont tous la meme longueur.
- un carré possède à la fois les propriétés d'un  rectangle et d'un losange.
- les autres parallèpipèdes seront considérés quelconques.

Pour définir un parallèpipède on dispose de sa longueur, sa largeur et d'un
angle formés par deux cotés. Par exemple, un parallépipède ayant une longueur
de **12**, une largeur de **8** et un angle de **90** sera un **rectangle**.
    
.. easypython:: exercices/devoir1/exo1.py
   :language: python
   :uuid: 1231313
