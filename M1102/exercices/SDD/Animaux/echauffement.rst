.. image:: exercices/SDD/Animaux/iguane.png 
   :align: left
   :height: 4em

Echauffement
------------
On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

``animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}``


Ecrire une fonction qui indique si un continent apparait dans le dictionnaire.

.. easypython:: exercices/SDD/Animaux/echauffement.py
   :language: python
   :uuid: 1231313

