.. image:: exercices/SDD/Animaux/hippo.png
   :align: left
   :height: 4em

Continents
----------

On dispose de deux dictionnaires :

* un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux. Par exemple :

``animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}``


* un autre dictionnaire indiquant, pour chaque animal, le nombre de représentants recensés. Par exemple :

``representants={'éléphant': 250, 'écureuil': 2000000, 'mésange': 2580, 'panda': 500, 'hippopotame': 890, 'girafe': 2580, 'iguane': 1450, 'lion': 1000}``



Ecrire une fonction qui, à partir de ces deux dictionnaires,
retourne pour chaque continent, les animaux qui y sont présents ainsi que le nombre de leurs représentants

.. easypython:: exercices/SDD/Animaux/continents.py
   :language: python
   :uuid: 1231313

