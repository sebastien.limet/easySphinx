.. image:: exercices/SDD/Animaux/elephant.png 
   :align: left
   :height: 4em

Recencement
------------
On dispose d'un dictionnaire dont les clefs sont des noms d'animaux, les valeurs sont les continents d'habitation de ces animaux.
Par exemple :

``animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}``


On veut écrire une fonction qui, à partir d'un tel dictionnaire,
retourne un dictionnaire avec le nombre d'espèces recensées par continent


.. easypython:: exercices/SDD/Animaux/echauffement.py
   :language: python
   :uuid: 1231313

