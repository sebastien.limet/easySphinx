animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}
representants={'éléphant': 250, 'écureuil': 2000000, 'mésange': 2580, 'panda': 500, 'hippopotame': 890, 'girafe': 2580, 'iguane': 1450, 'lion': 1000}

a={'a': 'A', 'b': 'B', 'c': 'A', 'd': 'D', 'e': 'A', 'f': 'D'}
r={'a':1, 'b':2, 'c':4, 'd':8, 'e':16, 'f':32}

entrees_visibles = [ (animaux, representants) ]
entrees_invisibles = [ (a, r) ]

@solution
def continents(animaux,representants) :
    dico={}
    for (animal, continent) in animaux.items():
        if continent in dico:
            dico[continent][animal]=representants[animal]
        else:
            dico[continent]={animal:representants[animal]}
    return dico

