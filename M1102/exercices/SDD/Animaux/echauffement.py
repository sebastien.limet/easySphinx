animaux={'éléphant': 'Asie', 'écureuil': 'Europe', 'panda': 'Asie', 'hippopotame': 'Afrique', 'girafe': 'Afrique', 'iguane': 'Amérique', 'lion': 'Afrique'}

a={'a': 'A', 'b': 'B', 'c': 'A', 'd': 'D', 'e': 'A', 'f': 'D'}

entrees_visibles = [ (animaux,'Afrique'), (animaux,'Océanie'), ]           

entrees_invisibles = [ (a,'B'), (a,'a'), (a,'D') ]

@solution
def nombreAnimauxParContinent(animaux, continent) :
    return continent in animaux.values()


