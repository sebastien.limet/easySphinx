Codage d'une lettre
-------------------

Écrire une fonction *codeLettre* qui permet de calculer le
code d'une lettre dans une clé donnée (voir explications plus haut).

.. easypython:: exercices/tpchaines/codeLettre.py
   :language: python
   :uuid: 1231313
