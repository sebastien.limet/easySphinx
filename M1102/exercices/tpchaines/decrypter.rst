Décryptage d'un texte
---------------------

En utilisant la fonction *decodeLettre*, écrire une fonction *decrypter* qui
permet de decrypter un texte suivant une clé donnée.
**ATTENTION** n'oubliez pas de recopier le code de la fonction *decodeLettre*

.. easypython:: exercices/tpchaines/decrypter.py
   :language: python
   :uuid: 1231313
