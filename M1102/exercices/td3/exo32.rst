Sous-chaînes
============

On voudrait extraire d'une chaîne la sous-chaîne à partir d'un caractère de 
numéro **debut** sur une longueur de **long** caractères. 
Par exemple, pour la chaîne *'bonjour'*, si le début est **2** et la longueur 
**3** on doit obtenir *'njo'*. Si le début n'est pas un indice de la chaîne, 
on doit retourner la chaîne vide et si la longueur est trop grande, 
on retourne la fin de la chaîne. 

.. easypython:: exercices/td3/exo32.py
   :language: python
   :uuid: 1231313
