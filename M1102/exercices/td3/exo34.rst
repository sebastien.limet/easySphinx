Comptage d'occurrences
======================

A partir d'une liste de nombres entiers positifs compris entre *0* et *N*, 
on aimerait construire une liste de longueur *N + 1* dont l'élément d'index 
*i* contient le nombre de fois que *i* aparaît dans la première liste. 

Par exemple, pour la liste [1, 0, 1, 3, 3, 1, 1] avec *N* vaut *3* on voudrait 
la liste [1, 4, 0, 2] car la première liste contient un 0, quatre 1, aucun 2 et 
deux 3. 

.. easypython:: exercices/td3/exo34.py
   :language: python
   :uuid: 1231313
