entrees_visibles = [("salutations", 3,),
                    ("bonjour", 2,),
                    ("sagesse", 3,)
]
entrees_invisibles = [
        ("", 2,),
        ("bonjour", 3,),
        ("lundi", 2,),
        ("abracadabra", 3,),
        ("soulagement", 3,),
]

@solution
def unSurN(chaineExtraire, nombre):
    """
    construit la chaine constituée des caractères de chaineExtraire pris tous 
    les nombre caractères.
    chaineExtraire est la chaine en entrée 
    nombre est le nombre qui détermine le pas d'extraction des caractères
    """ 
    res = ''
    for i in range(0, len(chaineExtraire), nombre):
        res += chaineExtraire[i]
    return res
