Inverser caractères
===================

On voudrait construire une chaîne à partir d'une autre en permuttant les 
lettres consécutives deux à deux. 
Par exemple avec la chaîne *'bonjour'* on devrait obtenir *'objnuor'*.  

.. easypython:: exercices/td3/exo33.py
   :language: python
   :uuid: 1231313
