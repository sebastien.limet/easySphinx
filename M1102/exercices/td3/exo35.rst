Permutation
===========
Une permutation permet de *mélanger* une liste d'éléments en suivant une certaine loi. 
Pour un ensemble à *N* éléments, toute liste de taille *N* qui contient tous 
les entiers de *0* à *N - 1* est une permutation. On voudrait à partir d'une 
liste de taille *N*, construire la liste mélangée suivant la permutation.

Par exemple, le mélange de la liste [12, 18, 23, 8] avec la permutation 
[3, 2, 0, 1] doit donner [23, 8, 18, 12] (12 va à la position 3, 18 à la 
position 2, 23 à la position 0 et 8 à la position 1).  



.. easypython:: exercices/td3/exo35.py
   :language: python
   :uuid: 1231313
