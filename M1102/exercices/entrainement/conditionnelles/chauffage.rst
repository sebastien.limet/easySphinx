Programmation du chauffage
--------------------------

J'ai programmé mes radiateurs de la façon suivante : 

   * Du lundi au vendredi, ils sont en MODE "Confort" de 6h à 8h et de 18h à 23h ; le reste du temps, ils sont en MODE "Eco". 
   * le samedi et le dimanche,  ils sont en MODE "Eco" jusqu'à 9h, puis après 23h ; le reste du temps, ils sont en MODE "Confort".

Ecrire une fonction qui prend en paramètre le jour de la semaine et une heure, 
et qui renvoie le MODE de mes radiateurs. 

On considère que les intervalles sont de la forme *[heureDebut, heureFin[*

.. easypython:: exercices/entrainement/conditionnelles/chauffage.py
   :language: python
   :uuid: 1231313