entrees_visibles = [("lundi",8),
                    ("lundi",9),
                    ("lundi",24),
                    ("dimanche",6),  
                    ("dimanche",9),                    
]
entrees_invisibles = [ (jour,heure) for jour in {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"} for heure in range(25)
]

@solution
def Mode (jour, heure) :
  if jour=="samedi" or jour=="dimanche":
    if 6<heure<=8 or 18<heure<=23:
      Mode="Confort"
    else:
      Mode="Eco"
  else:
    if 9<heure<=23:
      Mode="Confort"
    else:
      Mode="Eco"
  return Mode
